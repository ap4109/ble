const INITIAL_STATE = {
    BLEList: [],
    connectedDevice: {},
    status: 'disconnected'
  };
  const BLEReducer = (state =INITIAL_STATE, action) => {
    switch (action.type) {
      case 'ADD_BLE':
        if(state.BLEList.some(device => device.id === action.device.id) || !action.device.isConnectable || action.device.name === null){
          return state;
        } else {
          const newBLE = [
            ...state.BLEList,
            action.device
          ]
       return {
         BLEList: newBLE,
         connectedDevice: state.connectedDevice,
         status: action.status
        };
    }
    case 'CONNECTED_DEVICE':
      console.log("Reducer connected device", action);
      return {
        BLEList: state.BLEList,
        connectedDevice: action.connectedDevice,
        status: action.status
       };
    case 'CHANGE_STATUS':
      console.log("change status:", action.status)
      return {
        BLEList: state.BLEList,
        connectedDevice: action.connectedDevice,
        status: action.status}
      default:
        return state;
    }
  };
  
  export default BLEReducer;
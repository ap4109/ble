import {PermissionsAndroid, Platform} from 'react-native'
export const addBLE = (device) => ({
    type: "ADD_BLE",
    device
})
export const connectedDevice = (device) => ({
    type: "CONNECTED_DEVICE",
    connectedDevice: device
});

export const changeStatus = (status) => ({
    type: "CHANGE_STATUS",
    status: status
})

export const startScan = () => {
    return (dispatch, getState, DeviceManager) => {
        const subscription = DeviceManager.onStateChange((state) => {
            if (state === 'PoweredOn') {
                dispatch(scan());
                subscription.remove();
            }
        }, true);
      };
}

const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'Location permission for bluetooth scanning',
          
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Location permission for bluetooth scanning granted');
        return true;
      } else {
        console.log('Location permission for bluetooth scanning revoked');
        return false;
      }
    } catch (err) {
      console.warn(err);
      return false;
    }
  };
  
  export const scan = () => {
    return async (dispatch, getState, DeviceManager) => {
      const permission = Platform.OS === 'ios'? true: await requestLocationPermission();
      if (permission) {
        DeviceManager.startDeviceScan(null, null, (error, device) => {
          dispatch(changeStatus('Scanning'));
          if (error) {
            console.log(error);
          }
          if (device !== null) {
            dispatch(addBLE(device));
          }
        });
      } else {
        console.log('Error permission');
      }
    };
  };

export const connectDevice = (device) => {
    return (dispatch, getState, DeviceManager) => {
        
           dispatch(changeStatus("Connecting"));
           DeviceManager.stopDeviceScan()
          
            device.connect()
             , (error) => {
                console.log(this._logError("SCAN", error));
                return null;
              }
    }
}


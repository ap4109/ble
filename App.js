import React from 'react';
import { Provider } from 'react-redux';
import { createStore,applyMiddleware } from 'redux';
import rootReducer from './Redux/reducers';
import thunk from 'redux-thunk';
import {BleManager} from 'react-native-ble-plx';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import BLEList from './screens/BLElist'
const Stack = createStackNavigator();
function App(){
return(
  <Provider store={ store }>
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen name= 'Home' component ={BLEList} />
    </Stack.Navigator>
  </NavigationContainer>
  </Provider>
)
}
const DeviceManager = new BleManager();
const store = createStore(rootReducer, applyMiddleware(thunk.withExtraArgument(DeviceManager)));
export default App;
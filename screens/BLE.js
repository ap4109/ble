import React from 'react';
import { connect } from 'react-redux';
import {Text, View} from 'react-native';
class BLE extends React.Component {
    constructor(props) {
        super(props);
    };

    render() {
        return ( 
            <View>
                <Text style ={{fontSize: 18, paddingHorizontal:10}}>
                    Status: {this.props.status} 
                </Text>
                {this.props.connectedDevice && <Text style ={{fontSize: 18, paddingHorizontal:10}}>Device: {this.props.connectedDevice.name}</Text>}
            </View>
        );
    }
}

function mapStateToProps(state){
  return{
    BLEList : state.BLEs.BLEList,
    connectedDevice: state.BLEs.connectedDevice,
    status: state.BLEs.status
  };
}

const mapDispatchToProps = dispatch => ({
  addBLE: device => dispatch(addBLE(device))
})

export default connect(mapStateToProps,mapDispatchToProps,null,{ forwardRef: true })(BLE);
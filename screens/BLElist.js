import React, { Component } from 'react';
import { StyleSheet, Text, FlatList, View } from 'react-native';
import BLE from './BLE';
import { connect } from 'react-redux';
import { connectDevice, startScan } from '../Redux/actions/index';
import { TouchableOpacity } from 'react-native-gesture-handler';
class BLEList extends Component {
  constructor(props) {
    super(props);
    this.props.startScan();
  }
  handleClick = (device) => {
    this.props.connectDevice(device);
  }
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.BLEList}
          renderItem={({ item }) =>

            <TouchableOpacity onPress={() => this.handleClick(item)}>

              <Text> Tap to connect to: {item.name} </Text>

            </TouchableOpacity>

          }
          keyExtractor={item => item.id.toString()}
        />
        <View style={{ backgroundColor: '#bc85c7', height: 50, width: '100%' }}>
          <BLE/>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    BLEList: state.BLEs['BLEList']
  };
}

const mapDispatchToProps = dispatch => ({
  connectDevice: device => dispatch(connectDevice(device)),
  startScan: () => dispatch(startScan())
})

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#79b589',
    flex: 1,
  },

});

export default connect(mapStateToProps, mapDispatchToProps)(BLEList);